cmake_minimum_required(VERSION 3.20 FATAL_ERROR)
set(CMAKE_CXX_FLAGS "-Wall -std=c++2a")

project(concurrency)

add_executable(hello_world "code/hello_world.cpp")
add_executable(thread_transfer "code/thread_transfer.cpp")
add_executable(mutex_example "code/mutex_example.cpp")
# set_property(TARGET concurrency PROPERTY CXX_STANDARD 20)
# set_property(TARGET concurrency PROPERTY CXX_STANDARD_REQUIRED ON)
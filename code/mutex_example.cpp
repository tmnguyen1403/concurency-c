#include <list>
#include <mutex>
#include <algorithm>
#include <thread>
#include <iostream>

std::list<int> some_list;
std::mutex some_mutex;

void add_to_list(int new_value)
{
    std::lock_guard<std::mutex> guard(some_mutex);//mutually exclusive
    //std::lock_guard guard(some_mutex); //C++17 class template argument deduction
    //std::scoped_lock guard(some_mutex); //enhanced version
    some_list.push_back(new_value);
}

bool list_contains(int value_to_find)
{
    std::lock_guard<std::mutex> guard(some_mutex);
    return std::find(some_list.begin(),
    some_list.end(),
    value_to_find) != some_list.end();
}

int main()
{
    int value = 5;

    std::thread t1([&value]{
        if (list_contains(value)) {
            std::cout << "found value: " << value << std::endl;
        } else {
            std::cout << "not found: " << value << std::endl;
        }
    });
    t1.join();
    add_to_list(value);
    std::thread t2(add_to_list, 10);
    std::thread t3([&value]{
        if (list_contains(value)) {
            std::cout << "found value: " << value << std::endl;
        } else {
            std::cout << "not found: " << value << std::endl;
        }
         if (list_contains(10)) {
            std::cout << "found value: " << 10 << std::endl;
        } else {
            std::cout << "not found: " << 10 << std::endl;
        }
    });
    t2.join();
    t3.join();
}

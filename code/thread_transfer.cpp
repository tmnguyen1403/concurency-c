#include <thread>
#include <iostream>
#include <memory>

void dosth(int i){
    std::cout <<"do something in t1: " << i;
}
std::thread return_thread()
{
    return std::thread([] {std::cout<<"this thread is return\n";});
}

void accept_thread(std::thread t)
{
    if (t.joinable())
    {
        t.join();
    }
}

int main()
{
    std::cout << "hardware concurrency, how many threads can I run: " << std::thread::hardware_concurrency() << std::endl;
    std::thread t1(dosth, 10);
    std::thread t2=std::move(t1);
    t1 = std::thread([]{std::cout<< "test lambda\n";});
    std::thread t3;
    t3 = std::move(t2);
    t1.join();
    t3.join();
    //t1 = std::move(t3);//this assignment will terminate the program since t1 is attached to a thread

    //demo return and pass thread
    auto t4 = return_thread();
    //t4.join();
   // accept_thread(t4); // this will not work since the copy constructor for std::thread is deleted
    accept_thread(std::move(t4));
    return 0;
}
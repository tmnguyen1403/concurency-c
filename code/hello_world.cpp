#include <iostream>
#include <thread>
#include <vector>
#include <tuple>
#include <map>

void hello()
{
    std::cout<< "Hello Concurrent World\n";
}

namespace bb {
    struct tt {
        public:
         int c;
         tt(){c = 1;};
    };
};

int main()
{
    // std::thread t(hello);
    // t.join();
    using vector_t = std::vector<std::tuple<int,bb::tt>>;
    std::map<int, vector_t>a;
    vector_t t;
    t.push_back(std::tuple<int,bb::tt>(1,bb::tt()));
    a.emplace(0,t);
}